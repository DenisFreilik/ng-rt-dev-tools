/**
 * Gulp tasks
 */
'use strict';

const gulp = require('gulp');
require('./index').defineGulpTasks(gulp);
