'use strict';
const path = require('path');
const utils = require('../../utils/utils');
const logger = require('log4js').getLogger('cli.upload');
const i18n = require('i18n');
const fs = require('fs');
let fileData;
module.exports = parameters => {
  return new Promise((resolve, reject) => {
    //logger.info(i18n.__('upload plugin'));
    let filePath = path.resolve(parameters.baseDir, 'dist', parameters.name + '.zip');
    // logger.info('read file', filePath);
    fs.readFile(filePath, (err, data) => {
      if (err) return reject(err);
      //logger.info('file readed');
      fileData = data;
      // logger.info('parameters', parameters);
      utils.authorize(parameters)
        .then(authData => utils.sendRequest({
          host: parameters.host,
          port: parameters.port,
          url: '/ng-rt-core/plugins/app/remove',
          authToken: authData.token,
          data: {
            pluginName: parameters.pluginName,
            storage: parameters.storage
          }
        })).then(() => resolve(i18n.__('Plugin removed')))
        .catch(err => reject(i18n.__("Remove error: %s", err ? (typeof err === 'string' ? err : err.message) : 'unknown')));
    });
  });
}
