'use strict';
const path = require('path');
const utils = require('../../utils/utils');
const logger = require('log4js').getLogger('cli.upload');
const i18n = require('i18n');
const fs = require('fs');

const readFileCB = (resolve, reject, parameters) => (err, fileData) => {
  if (err) return reject(err);
  logger.debug('parameters.host :', parameters.host);
  logger.debug('parameters.port :', parameters.port);

  return utils.authorize(parameters)
    .then(authData => utils.sendRequest({
      host: parameters.host,
      port: parameters.port,
      url: '/ng-rt-core/plugins/app/zipurl',
      authToken: authData.token,
      data: {
        fileContent: fileData.toString('binary'),
        url: parameters.name + '.zip'
      }
    }))
    .then(() => resolve(i18n.__('Uploading done')))
    .catch(
      err => reject(i18n.__("Uploading error: %s", err ? (typeof err === 'string' ? err : err.message) : 'unknown'))
    );
};

// cb param will be mocked for tests
const promiseExecutor = (parameters, cb = readFileCB) => (resolve, reject) => {
  let filePath = path.resolve(parameters.baseDir, 'dist', parameters.name + '.zip');
  fs.readFile(filePath, cb(resolve, reject, parameters));
};

// cb param will be mocked for tests
const uploadFile = (parameters, executor = promiseExecutor) => {
  return new Promise(executor(parameters));
};

module.exports = {
  default: uploadFile,
  readFileCB,
  promiseExecutor
};
