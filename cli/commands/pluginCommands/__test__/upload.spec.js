'use strict';
const proxyquire = require('proxyquire');
const expect = require('chai').expect;
const sinon = require('sinon');

const pathMock = {
  resolve: sinon.stub()
};
const fsMock = {
  readFile: sinon.stub()
};
const utilsMock = {
  authorize: sinon.stub(),
  sendRequest: sinon.stub()
};
const i18nMock = {
  __: sinon.stub()
};

let {default: uploadFile, readFileCB, promiseExecutor} = proxyquire('../upload', {
  "path": pathMock,
  "fs": fsMock,
  "log4js": {
    getLogger() {
      return {
        debug() {}
      };
    }
  },
  "i18n": i18nMock,
  '../../utils/utils': utilsMock
});

describe('In upload.js:', () => {
  let parameters;
  const callback = sinon.stub();
  const authData = {};

  beforeEach(() => {
    parameters = {
      baseDir: Math.random(),
      name: Math.random(),
      host: Math.random(),
      port: Math.random()
    };
    pathMock.resolve.reset();
    pathMock.resolve.returns(Math.random());
    fsMock.readFile.reset();
    fsMock.readFile.returns(Math.random());
    utilsMock.authorize.reset();
    authData.token = Math.random();
    utilsMock.authorize.returns(Promise.resolve(authData));
    utilsMock.sendRequest.reset();
    i18nMock.__.reset();
    i18nMock.__.returns(Math.random());
    callback.reset();
    callback.returns(() => {});
  });

  describe('uploadFile function', () => {
    it('SHOULD return Promise', () => {
      expect(uploadFile(parameters, callback)).to.be.an.instanceof(Promise);
    });

    it('SHOULD pass parameters to Promise executor function', () => {
      expect(callback.notCalled).to.be.true;
      uploadFile(parameters, callback);
      expect(callback.calledOnce).to.be.true;
      expect(callback.args[0][0]).to.be.deep.equal(parameters);
    });
  });

  describe('promiseExecutor function', () => {
    it('SHOULD resolve file correctly', () => {
      expect(pathMock.resolve.notCalled).to.be.true;
      promiseExecutor(parameters, callback)();
      expect(pathMock.resolve.calledOnce).to.be.true;
      expect(pathMock.resolve.args[0]).to.be.deep.equal([parameters.baseDir, 'dist', parameters.name + '.zip']);
    });

    it('SHOULD pass parameters and Promise callbacks to fs.readFile\'s callback function', () => {
      const resolve = Math.random();
      const reject = Math.random();
      expect(callback.notCalled).to.be.true;
      promiseExecutor(parameters, callback)(resolve, reject);
      expect(callback.calledOnce).to.be.true;
      expect(callback.args[0]).to.be.deep.equal([resolve, reject, parameters]);
    });

    it('SHOULD read file correctly', () => {
      expect(fsMock.readFile.notCalled).to.be.true;
      const filePath = pathMock.resolve();
      promiseExecutor(parameters, callback)();
      expect(fsMock.readFile.calledOnce).to.be.true;
      expect(fsMock.readFile.args[0]).to.be.deep.equal([filePath, callback()]);
    });
  });

  describe('fs.readFile\'s callback function', () => {
    const resolve = sinon.spy();
    const reject = sinon.spy();
    let fileBinaryData;
    const fileData = {
      toString: sinon.spy(encoding => {
        if (encoding === 'binary') return fileBinaryData;
        throw Error('Wrong encoding format for file data!');
      })
    };

    beforeEach(() => {
      resolve.reset();
      reject.reset();
      fileBinaryData = Math.random();
      fileData.toString.reset();
    });

    it('SHOULD reject error, if there was error while reading the file', () => {
      const err = Math.random();
      expect(reject.notCalled).to.be.true;
      readFileCB(resolve, reject, null)(err, null);
      expect(reject.calledOnce).to.be.true;
      expect(reject.args[0][0]).to.be.equal(err);
    });

    it('SHOULD pass parameters to utils.authorize method', () => {
      expect(utilsMock.authorize.notCalled).to.be.true;
      readFileCB(resolve, reject, parameters)(null, fileData);
      expect(utilsMock.authorize.calledOnce).to.be.true;
      expect(utilsMock.authorize.args[0][0]).to.be.deep.equal(parameters);
    });

    it('SHOULD send request with correct config', done => {
      expect(utilsMock.sendRequest.notCalled).to.be.true;
      readFileCB(resolve, reject, parameters)(null, fileData)
        .then(() => {
          expect(utilsMock.sendRequest.calledOnce).to.be.true;
          expect(utilsMock.sendRequest.args[0][0]).to.be.deep.equal({
            host: parameters.host,
            port: parameters.port,
            url: '/ng-rt-core/plugins/app/zipurl',
            authToken: authData.token,
            data: {
              fileContent: fileBinaryData,
              url: parameters.name + '.zip'
            }
          });

          done();
        })
        .catch(err => {
          done(err);
        });
    });

    it('SHOULD resolve correct message if uploading was successful', done => {
      expect(resolve.notCalled).to.be.true;
      expect(i18nMock.__.notCalled).to.be.true;
      readFileCB(resolve, reject, parameters)(null, fileData)
        .then(() => {
          expect(i18nMock.__.calledOnce).to.be.true;
          expect(i18nMock.__.args[0][0]).to.be.equal('Uploading done');
          expect(resolve.calledOnce).to.be.true;
          expect(resolve.args[0][0]).to.be.equal(i18nMock.__());
          done();
        })
        .catch(err => {
          done(err);
        });
    });

    it('SHOULD reject correct message if uploading wasn\'t successful', done => {
      expect(reject.notCalled).to.be.true;
      expect(i18nMock.__.notCalled).to.be.true;
      const error = {
        message: Math.random()
      };
      utilsMock.sendRequest.throws(error);
      readFileCB(resolve, reject, parameters)(null, fileData)
        .then(() => {
          expect(i18nMock.__.calledOnce).to.be.true;
          expect(i18nMock.__.args[0]).to.be.deep.equal(['Uploading error: %s', error.message]);
          expect(reject.calledOnce).to.be.true;
          expect(reject.args[0][0]).to.be.equal(i18nMock.__());
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
});
