const path = require('path');
const logger = require('log4js').getLogger('activate');
const configService = require('../../server/backend/configService');

module.exports = action => {
  /**
   * creates all required files for starting the server
   * @param {object} argv - The object instance of the type ARGV
   * @param {Object} output - output stream
   * @param {Object} i18n - i18n interface
   * @return {Promise} on command finish
   */
  function command(argv, output, i18n) {
    return new Promise((resolve, reject) => {
      let baseDir = path.resolve(__dirname, '../../../../');
      let name = path.basename(path.resolve(__dirname, '../../../../'));
      let environments;
      let curEnv;
      let urlParts;
      let actionPromises = require(path.resolve(__dirname, action));
      let parameters = {};
      environments = configService.get('env');
      curEnv = argv.serverEnv || environments.default;
      if (!environments.default || !environments[environments.default]) {
        return reject('Default environment is not defined');
      }

      /**
       * @typedef {Object} env
       * @property {string} serverUrl
       */
      curEnv = typeof environments[curEnv] === 'object' ? environments[curEnv] : environments[environments.default];

      urlParts = curEnv.serverUrl.split(':');

      parameters.host = argv.host || urlParts[0];
      parameters.port = argv.port || urlParts[1] || 80;
      parameters.appKey = argv.appKey || curEnv.appKey;
      parameters.appId = argv.appId || curEnv.appId || "ng-rt-admin";
      parameters.pluginName = argv.pluginName || name;
      parameters.storage = argv.storage || curEnv.storage;
      parameters.baseDir = baseDir;
      parameters.name = name;
      return actionPromises(parameters).then(resolve).catch(reject);
    });
  }
  return command;
}
