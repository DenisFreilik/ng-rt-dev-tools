const sendRequest = require('./sendRequest');

module.exports = parameters => {
  return sendRequest({
    host: parameters.host,
    port: parameters.port,
    url: '/auth/applogin',
    readData: true,
    data: {
      appID: parameters.appId,
      appKey: parameters.appKey
    }
  });
}