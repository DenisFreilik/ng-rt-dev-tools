const http = require('http');
const logger = require('log4js').getLogger('cli.upload');

/**
 * Common function to send requests to server
 * @param {Object} conf - object with configuration data
 * @param {*} conf.host - host to send request
 * @param {*} conf.port - port of host to send request
 * @param {*} conf.url - url to send request
 * @param {*} conf.authToken - JWT auth token
 * @param {*} conf.readData - flag, used for force reading data from server response
 * @param {Object} conf.data - data, sended to server
 * @return {Object} Promise
 */
function sendRequest(conf) {
    return new Promise((resolve, reject) => {
      let options = {
        host: conf.host,
        port: conf.port,
        path: conf.url,
        method: 'POST',
        headers: {
          'Accept': '*/*',
          'Accept-Encoding': 'deflate',
          'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
          'Cache-Control': 'no-store,no-cache,must-revalidate,post-check=0,pre-check=0,max-age=1,s-maxage=1',
          'Charset': 'utf-8',
          'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36',
          'Connection': 'close',
          'Content-Type': 'application/json'
        }
      };

      logger.debug('conf.authToken :', conf.authToken);

      if (conf.authToken) {
        options.headers.Authorization = 'JWT ' + conf.authToken;
      }
      let req = http.request(options, res => {
        if (res.statusCode !== 200) {
          return reject(`Wrong status code ${res.statusCode}: res`);
        }
        if (conf.readData) {
          res.setEncoding('utf8');
          res.on('data', function (chunk) {
            resolve(JSON.parse(chunk));
          });
        } else {
          resolve();
        }
      });

      req.write(JSON.stringify(conf.data));
      req.end();
    });
  }

  module.exports = sendRequest;
