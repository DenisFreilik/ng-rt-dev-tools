module.exports = {
    authorize: require('./authorize'),
    sendRequest: require('./sendRequest')
}