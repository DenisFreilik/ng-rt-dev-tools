/**
 * Main file for ng-rt platform development.
 * This nodejs module should be available globally or locally.
 */
'use strict';
const path = require('path');
const _ = require('lodash');

const defineGulpTasks = (gulp, options) => {
  // we use gulp instance provided by a module via parameter
  // but if there is no ref. to gulp, then we use
  if (!gulp)
    gulp = require("gulp");

  // from the lowest priority to the highest
  options = _.extend(
    // default options
    {
      name: path.basename(path.dirname(module.parent.filename)),
      baseDir: path.dirname(module.parent.filename),
      browserify: {},
      vulcanize: {},
      userguides: {}
    },

    // options from parameters
    options,

    // options provided via command line
    require('minimist')(process.argv.slice(2), {
      string: ['src']
    })
  );

  require('./gulp_tasks/doc')(gulp, options);
  require('./gulp_tasks/userguides')(gulp, options);
  require('./gulp_tasks/lint')(gulp, options);

  const testTasks = require('./gulp_tasks/test')(gulp, options);
  const buildTasks = require('./gulp_tasks/build')(gulp, options);

  return {
    mocha: testTasks.mocha,

    vulcanize: buildTasks.vulcanize,
    copyToPublic: buildTasks.copyToPublic,
    pugToPublic: buildTasks.pugToPublic,
    bundleJs: buildTasks.bundleJs,
    copyJs: buildTasks.copyJs
  };
};

module.exports = {
  defineGulpTasks: defineGulpTasks,
  eslintrcPath: path.resolve(__dirname, '.eslintrc.js'),
  baseDir: path.resolve(__dirname, '../..')
};
