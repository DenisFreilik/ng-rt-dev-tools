"use strict";

const Through = require('through2');
const Cheerio = require('cheerio');
const Babel = require('babel-core');
const Gutil = require('gulp-util');

module.exports = opts => {
  opts = opts || {
    babel: {}
  };

  return Through.obj((file, enc, cb) => {
    if (file.isNull()) {
      return cb(null, file);
    }

    let dom = Cheerio.load(file.contents.toString());

    let scripts = dom('script:not([src]):not([data-flag-external])');

    scripts.each((i, el) => {
      let code = dom(el).text();
      let codeProcessed = code;

      Gutil.log('Script ' + (i + 1) + ' of ' + scripts.length + ' start babel processing, len: ' + code.length);

      try {
        codeProcessed = Babel.transform(code, opts.babel).code;
      } catch (e) {
        console.error(e);
        codeProcessed = code;
      }

      dom(el).text(codeProcessed);
    });

    file.contents = new Buffer(dom.html());

    cb(null, file);
  });
};