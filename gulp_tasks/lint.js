/**
 * ESLinting task:
 * lint
 *    eslint current project
 */
'use strict';

const eslint = require('gulp-eslint');

module.exports = (gulp, options) => {
  gulp.task('lint', () =>
    gulp.src(options.src || [
      '**/*.js',
      '!**/*.min.js',
      '!node_modules/**',
      '!**/bower_components/**',
      '!**/public/**',
      '!plugins/**',
      '!docs/**',
      '!dist/**',
      '!config/docs/**',
      '!coverage/**'
    ])
      .pipe(eslint({
        fix: true
      }))
      .pipe(eslint.format())
      .pipe(gulp.dest(function(file) {
        return file.base;
      }))
      .pipe(eslint.failAfterError())
  );
};
