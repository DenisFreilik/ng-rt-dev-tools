/**
 * Tests running.
 *
 * tasks:
 *
 * test.plugins
 * test.plugins.unit
 * test.plugins.server
 * test.plugins.ui
 * test.plugins.nm
 *
 * test.core
 * test.core.unit
 * test.core.server
 * test.core.ui
 * test.core.nm
 *
 * test.all
 * test.all.unit
 * test.all.server
 * test.all.ui
 * test.all.nm
 *
 * test
 * test.unit
 * test.server
 * test.ui
 * test.nm
 *
 * methods:
 *
 * mocha
 */
'use strict';
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const istanbul = require('gulp-istanbul');

module.exports = (gulp, options) => {
  const mocha = require('gulp-mocha');

  const testError = err => {
    console.error(err.message);
    console.error(err.stack);
    process.exit(1);
  };

  const testEnd = () => {
    process.exit();
  };

  gulp.task('pre.test', function() {
    return gulp.src([
        'server/**/*.js',
        'api/**/*.js',
        'shared/**/*.js',
        'pluginManager/**/*.js'
      ])
      .pipe(istanbul({includeUntested:true}))
      .pipe(istanbul.hookRequire());
  });

  const defineMochaTask = (taskName, src, mochaOptions) => {
    mochaOptions = _.defaults({}, mochaOptions, {
      timeout: options.timeout || 1800000 // 30 minutes
    });
    gulp.task(taskName, ['pre.test'], () =>
      gulp.src(src)
        .pipe(mocha(mochaOptions))
        .pipe(istanbul.writeReports())
        .once('error', testError)
        .once('end', testEnd)
    );
    return taskName;
  };

  const unitTasks = [];
  const serverTasks = [];
  const uiTasks = [];
  const nmTasks = [];

  const pluginsDir = path.resolve('plugins');
  if (fs.existsSync(pluginsDir))
    fs.readdirSync(pluginsDir).forEach(pluginType => {
      if (!(fs.lstatSync(path.resolve('plugins', pluginType))).isDirectory()) {
        return;
      }
      const pluginUnitTasks = [];
      const pluginServerTasks = [];
      const pluginUiTasks = [];
      const pluginNmTasks = [];
      fs.readdirSync(path.resolve('plugins', pluginType)).forEach(plugin => {
        if (!(fs.lstatSync(path.resolve('plugins', pluginType, plugin))).isDirectory()) {
          return;
        }
        const taskBase = 'test.plugins.' + pluginType + '.' + plugin;
        const unitTask = taskBase + '.unit';
        const serverTask = taskBase + '.server';
        const uiTask = taskBase + '.ui';
        const nmTask = taskBase + '.nm';
        const pathBase = 'plugins/' + pluginType + '/' + plugin;
        const unitPath = pathBase + '/test/unit/**/*_unit.js';
        const serverPath = pathBase + '/test/server/**/*_test.js';
        const uiPath = pathBase + '/test/ui/**/*_test.js';
        const nmPath = pathBase + '/test/nm/**/*_test.js';
        defineMochaTask(taskBase + '.unit', unitPath);
        defineMochaTask(taskBase + '.server', serverPath);
        defineMochaTask(taskBase + '.ui', uiPath);
        defineMochaTask(taskBase + '.nm', uiPath);
        gulp.task(taskBase, [unitTask, serverTask, uiTask]);
        pluginUnitTasks.push(unitTask);
        pluginServerTasks.push(serverTask);
        pluginUiTasks.push(uiTask);
      });
      const taskBase = 'test.plugins.' + pluginType;
      const unitTask = 'test.plugins.' + pluginType + '.unit';
      const serverTask = 'test.plugins.' + pluginType + '.server';
      const uiTask = 'test.plugins.' + pluginType + '.ui';
      const nmTask = 'test.plugins.' + pluginType + '.nm';
      gulp.task(unitTask, pluginUnitTasks);
      gulp.task(serverTask, pluginServerTasks);
      gulp.task(uiTask, pluginUiTasks);
      gulp.task(nmTask, pluginNmTasks);
      gulp.task(taskBase, [].concat(pluginUnitTasks, pluginServerTasks, pluginUiTasks, pluginNmTasks));
      unitTasks.push(unitTask);
      serverTasks.push(serverTask);
      uiTasks.push(uiTask);
      uiTasks.push(nmTask);
    });

  gulp.task('test.plugins.unit', unitTasks);
  gulp.task('test.plugins.server', serverTasks);
  gulp.task('test.plugins.ui', uiTasks);
  gulp.task('test.plugins.nm', nmTasks);
  gulp.task('test.plugins', [].concat(unitTasks, serverTasks, uiTasks, nmTasks));

  defineMochaTask('test.core.unit', 'test/unit/**/*_unit.js');
  defineMochaTask('test.core.server', 'test/server/**/*_test.js');
  defineMochaTask('test.core.ui', 'test/ui/**/*_test.js');
  defineMochaTask('test.core.nm', 'test/nm/**/*_test.js');
  gulp.task('test.core', ['test.core.unit', 'test.core.server', 'test.core.ui', 'test.core.nm']);

  gulp.task('test.all.unit', ['test.plugins.unit', 'test.core.unit']);
  gulp.task('test.all.server', ['test.plugins.server', 'test.core.server']);
  gulp.task('test.all.ui', ['test.core.ui', 'test.plugins.ui']);
  gulp.task('test.all.nm', ['test.core.nm', 'test.plugins.nm']);
  gulp.task('test.all', ['test.all.unit', 'test.all.server', 'test.all.ui', 'test.all.nm']);

  defineMochaTask('test.unit', 'test/unit/**/*_unit.js');
  defineMochaTask('test.server', 'test/server/**/*_test.js');
  defineMochaTask('test.ui', 'test/ui/**/*_test.js');
  defineMochaTask('test.nm', 'test/nm/**/*_test.js');
  defineMochaTask('test', options.src || 'test/unit/**/*_unit.js');
  defineMochaTask('test.full', ['test/unit/**/*_unit.js', 'test/server/**/*_test.js', 'test/ui/**/*_test.js', 'test/nm/**/*_test.js']);

  return {
    mocha: defineMochaTask
  };
};
