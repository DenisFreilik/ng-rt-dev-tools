/**
 * Documentation tasks:
 * userguides
 *    creates SPHINX based documentation in ./dist/docs/
 *
 */
'use strict';
const fs = require('fs');

module.exports = (gulp, options) => {

  const gulpsync = require('gulp-sync')(gulp);
  const shell = require('gulp-shell');
  const del = require('del');
  const distDir = 'docs/dist';
  const buildDir = 'docs/build';
  const buildDirAdmin = 'docs/admin/build';
  const buildDirEnduser = 'docs/enduser/build';
  const _ = require('lodash');
  const buildTasks = [];
  const copyTasks = [];

  let userguides = options.userguides.includes;
  let outputHtml = options.userguides.outputHtml == false ? false : true; // if no entry default TRUE
  let outputEpub = options.userguides.outputEpub == false ? false : true;
  let outputPdf = options.userguides.outputPdf == false ? false : true;

  var defineHtmlBuildTask = (taskName) => {
    gulp.task(`build-html-${taskName}`, shell.task(`make html`, {
      cwd: `./docs/${taskName}`
    }))
    buildTasks.push(`build-html-${taskName}`);
  };

  var defineEpubBuildTask = (taskName) => {
    gulp.task(`build-epub-${taskName}`, shell.task(`make epub`, {
      cwd: `./docs/${taskName}`
    }))
    buildTasks.push(`build-epub-${taskName}`);
  };

  var definePdfBuildTask = (taskName) => {
    gulp.task(`build-pdf-${taskName}`, shell.task(`make latexpdf`, {
      cwd: `./docs/${taskName}`
    }))
    buildTasks.push(`build-pdf-${taskName}`);
  };

  var defineHtmlCopyTask = (taskName) => {
    gulp.task(`copy-html-${taskName}`, () =>
      gulp.src(`docs/${taskName}/build/html/**/*`)
      .pipe(gulp.dest(`docs/dist/userguides/${taskName}/html`)));
    copyTasks.push(`copy-html-${taskName}`);
  };

  var defineEpubCopyTask = (taskName) => {
    gulp.task(`copy-epub-${taskName}`, () =>
      gulp.src(`docs/${taskName}/build/epub/**/*`)
      .pipe(gulp.dest(`docs/dist/userguides/${taskName}/epub`)));
    copyTasks.push(`copy-epub-${taskName}`);
  };

  var definePdfCopyTask = (taskName) => {
    gulp.task(`copy-pdf-${taskName}`, () =>
      gulp.src(`docs/${taskName}/build/latex/**/*`)
      .pipe(gulp.dest(`docs/dist/userguides/${taskName}/pdf`)));
    copyTasks.push(`copy-pdf-${taskName}`);
  };

  if (outputHtml == true) _(userguides).forEach(defineHtmlBuildTask);
  if (outputEpub == true) _(userguides).forEach(defineEpubBuildTask);
  if (outputPdf == true) _(userguides).forEach(definePdfBuildTask);

  _(userguides).forEach(defineHtmlCopyTask);
  _(userguides).forEach(defineEpubCopyTask);
  _(userguides).forEach(definePdfCopyTask);

  gulp.task('clean-all', gulpsync.sync(['clean-dist', 'clean-build']));

  // Clean Build folder
  gulp.task('clean-build', callback => {
    del.sync([buildDir, buildDirAdmin, buildDirEnduser]);
    callback();
  });

  gulp.task('clean-dist', callback => {
    del.sync([distDir]);
    callback();
  });

  gulp.task('userguides', gulpsync.sync(['clean-all', buildTasks, copyTasks]));

};
