'use strict';

const gutil = require('gulp-util');
const through = require('through2');
const Vulcanize = require('vulcanize');
const dom5 = require('dom5');
const url = require('url');
const matchers = require('../../vulcanize/lib/matchers');
const encodeString = require('../../vulcanize/third_party/UglifyJS2/output');
const Babel = require('babel-core');

let babelProcessFiles = [];
let babelOpts = {};

const processThroughBabel = src => {
  for (let i = 0, l = babelProcessFiles.length; i < l; ++i) {
    if (babelProcessFiles[i] instanceof RegExp) {
      if (babelProcessFiles[i].test(src)) {
        return true;
      }
    }
    if (babelProcessFiles[i].indexOf(src) !== -1) {
      return true;
    }
  }
  return false;
};

Vulcanize.prototype.inlineScripts = function inlineScripts(doc, href) {
  var scripts = dom5.queryAll(doc, matchers.JS_SRC);
  var scriptPromises = scripts.map(function(script) {
    var src = dom5.getAttribute(script, 'src');
    var uri = url.resolve(href, src);
    // let the loader handle the requests
    if (this.isExcludedHref(src)) {
      return Promise.resolve(true);
    }
    return this.loader.request(uri).then(function(content) {
      if (content) {
        content = encodeString(content);

        if (processThroughBabel(src)) {
          gutil.log('Script ' + src + ' start babel processing, len: ' + content.length);
          try {
            content = Babel.transform(content, babelOpts).code;
          } catch (e) { }
        }

        dom5.removeAttribute(script, 'src');
        dom5.setAttribute(script, 'data-flag-external', 'true');
        dom5.setAttribute(script, 'data-external-src', src);
        dom5.setTextContent(script, content);
      }
    });
  }.bind(this));
  // When all scripts are read, return the document
  return Promise.all(scriptPromises).then(function() {
    return {doc: doc, href: href};
  });
};

module.exports = function(opts) {
  opts = opts || {};

  babelProcessFiles = opts.babelProcessFiles || [];
  babelOpts = opts.babel || {};

  return through.obj(function(file, enc, cb) {
    if (file.isNull()) {
      cb(null, file);
      return;
    }

    if (file.isStream()) {
      cb(new gutil.PluginError('gulp-vulcanize', 'Streaming not supported'));
      return;
    }

    (new Vulcanize(opts)).process(file.path, function(err, inlinedHtml) {
      if (err) {
        cb(new gutil.PluginError('gulp-vulcanize', err, {fileName: file.path}));
        return;
      }

      file.contents = new Buffer(inlinedHtml);
      cb(null, file);
    });
  });
};
