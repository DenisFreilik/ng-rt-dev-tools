/**
 * Documentation tasks:
 * api
 *    creates jsdoc documentation in ./dist/docs/api
 *
 */
'use strict';
const fs = require('fs');
const jsdoc = require('gulp-jsdoc3');
const rimraf = require('rimraf');

module.exports = (gulp, options) => {
  gulp.task('api', cb => {
    /**
     * Configuration file path
     */
    const CONFIG_FILE_PATH = './conf.json';

    /**
     * Documentation directory
     */
    const DOC_DIR = './dist/docs/api';

    /**
     * If we have config file then use it
     * Else define default configuration for jsdoc
     */
    let config = fs.existsSync(CONFIG_FILE_PATH) ? require(CONFIG_FILE_PATH) : {
      opts: {
        destination: DOC_DIR,
        recurse: true,
        private: true
      },
      tags: {
        allowUnknownTags: true,
        dictionaries: ["jsdoc", "closure"]
      },
      plugins: ["./node_modules/jsdoc-route-plugin"]
    };

    /**
     * Delete documentation directory
     */
    rimraf(DOC_DIR, () => {
      /**
       * Generate jsdoc documentation
       */
      gulp.src(['./manifest.json'].concat([
        './api/**/*.js',
        './server/**/*.js',
        './shared/**/*.js',
        './pluginManager/**/*.js',
        './utils/**/*.js',
        './client/src/js/**/*.js'
      ]), {
        read: false
      })
        .pipe(jsdoc(config, cb));
    });
  });
};
