/**
 * Building.
 *  - browserify shared code for client side (UI)
 *  - build pure client code
 *  - distribution package build
 *
 * tasks:
 *
 * buildShared
 * cleanClient
 * bower
 * less
 * pug
 * vulcanizeFiles
 * vulcanize
 * customBuildClient
 * buildClient
 * cleanDist
 * copyToDist
 * writeVersion
 * zip
 * clean
 * dist
 * build
 * rebuild
 *
 * methods:
 *
 * vulcanize
 * copyToPublic
 * pugToPublic
 * bundleJs
 * copyJs
 *
 */
'use strict';
const path = require('path');
const fs = require('fs');

module.exports = (gulp, options) => {
  const gulpsync = require('gulp-sync')(gulp);
  const zip = require('gulp-zip');
  const del = require('del');

  const sharedDir = path.join(options.baseDir, 'shared');
  const clientDir = path.join(options.baseDir, 'client');
  const clientPublicDir = path.join(clientDir, 'public');
  const clientBowerDir = path.join(clientDir, 'bower_components');
  const clientViewsDir = path.join(clientDir, 'src', 'views');

  // Browserify shared (server & UI) code

  const sourcemaps = require('gulp-sourcemaps');
  const source = require('vinyl-source-stream');
  const buffer = require('vinyl-buffer');
  const browserify = require('browserify');
  const babelify = require('babelify');

  /**
   * Index of the week of date.
   * @param {Date} date the date for getting its num. of the week
   * @return {number} Index of the weel
   * */
  function weekOfYear(date) {
    var d = new Date(Number(date));
    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
  }

  gulp.task('buildShared', () => {
    const indexPath = path.join(sharedDir, 'index.js');

    // if there is no index.js, then skip
    if (!fs.existsSync(indexPath))
      return Promise.resolve();

    return browserify(indexPath, {
      debug: true,
      noParse: options.browserify.noParse || []
    })
      .transform(babelify, {
        presets: [require('babel-preset-es2015')]
      })
      .bundle()
      .pipe(source('build.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({
        loadMaps: true
      }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(clientPublicDir));
  });

  // Build client code (UI)

  const vulcanize = require('./vulcanizator');
  const bower = require('gulp-bower');
  const less = require('gulp-less');
  const concat = require('gulp-concat');
  const cleanCSS = require('gulp-clean-css');
  const rename = require('gulp-rename');
  const pug = require('gulp-pug');
  const babel = require('gulp-babel');
  const wrap = require('gulp-wrap');

  gulp.task('cleanClient', callback => {
    del.sync([clientPublicDir, clientBowerDir, clientViewsDir]);
    callback();
  });

  const defineCopyToPublicTask = (taskName, srcPath, dstPath) => {
    srcPath = Array.isArray(srcPath) ? srcPath : [srcPath];
    srcPath = Array.map(srcPath, p => path.join(clientDir, p));
    gulp.task(taskName, () =>
      gulp.src(srcPath)
        .pipe(gulp.dest(path.join(clientPublicDir, dstPath)))
    );
    return taskName;
  };

  defineCopyToPublicTask('copyRes', 'src/res/**/*', 'res');

  const definePugToPublicTask = (taskName, srcPath, dstPath) => {
    srcPath = Array.isArray(srcPath) ? srcPath : [srcPath];
    srcPath = Array.map(srcPath, p => path.join(clientDir, p));
    gulp.task(taskName, () =>
      gulp.src(srcPath)
        .pipe(pug({
          basedir: 'client/src'
        }))
        .pipe(gulp.dest(path.join(clientPublicDir, dstPath)))
    );
    return taskName;
  };

  const defineBundleJsTask = (taskName, srcPath, jsFile) => {
    srcPath = Array.isArray(srcPath) ? srcPath : [srcPath];
    srcPath = Array.map(srcPath, p => path.join(clientDir, p));
    gulp.task(taskName, () =>
      gulp.src(srcPath)
        .pipe(babel({
          presets: [
            ['es2015', {
              modules: false,
              spec: false
            }]
          ],
          compact: false
        }))
        .pipe(concat(jsFile))
        .pipe(wrap('(function(){<%= contents %>\n}).call(this);'))
        .pipe(gulp.dest('client/public/js'))
    );
    return taskName;
  };

  const defineCopyJsTask = (taskName, srcPath) => {
    srcPath = Array.isArray(srcPath) ? srcPath : [srcPath];
    srcPath = Array.map(srcPath, p => path.join(clientDir, p));
    gulp.task(taskName, () =>
      gulp.src(srcPath)
        .pipe(babel({
          presets: [
            ['es2015', {
              modules: false,
              spec: false
            }]
          ],
          compact: false
        }))
        .pipe(gulp.dest('client/public/js'))
    );
    return taskName;
  };

  gulp.task('bower', () => {
    if (!fs.existsSync(path.join(clientDir, 'bower.json')))
      return Promise.resolve();
    return bower({
      cwd: clientDir
    });
  });

  gulp.task('less', () =>
    gulp.src(clientDir + '/src/styles/**/*.less')
      .pipe(less())
      .pipe(concat('_common.css'))
      .pipe(cleanCSS({
        processImport: false
      }))
      .pipe(rename({
        suffix: ".min"
      }))
      .pipe(gulp.dest(path.join(clientDir, 'src', 'styles')))
  );

  gulp.task('pug', () => {
    gulp.src(clientDir + '/src/pug/*.pug')
      .pipe(pug({
        basedir: clientDir + '/src'
      }))
      .pipe(gulp.dest(clientViewsDir));
  });

  /**
   * Define task for vulcanization specific file.
   * @param {string} taskName name of new gulp task.
   * @param {string} srcFilePath relative path to vulcanized file; relative to `client` dir.
   * @return {string} taskName
   */
  const defineVulcanizeTask = (taskName, srcFilePath) => {
    gulp.task(taskName, () => {
      let task = gulp.src(path.join(clientDir, srcFilePath))
        .pipe(vulcanize({
          abspath: '',
          inlineScripts: true,
          inlineCss: true,
          implicitStrip: true,
          stripComments: true,
          excludes: Array.concat(['build.js'], options.vulcanize.excludes || []),
          stripExcludes: false,
          strip: true,
          babelProcessFiles: options.babelProcessFiles || [],
          babel: {
            presets: [
              ['es2015', {
                modules: false,
                spec: false
              }]
            ],
            compact: false
          }
        }))
        .on("error", function(err) {
          console.log("gulp error: " + err);
        });
      if (options.babelProcessVulkanizedFiles) {
        task.pipe(require('./babelInlineProcessor')({
          babel: {
            presets: [
              ['es2015', {
                modules: false,
                spec: false
              }]
            ],
            compact: false
          }
        }));
      }
      return task.pipe(gulp.dest(clientPublicDir));
    });
    return taskName;
  };

  // define vulcanize tasks for regular html's and views created by pug templates
  const vulcanizeTasks = Array.concat(Array.map(options.vulcanize.src || ['index.html'], filename => {
    const taskName = 'vulc-src-' + filename;
    defineVulcanizeTask(taskName, path.join('src', filename));
    return taskName;
  }), Array.map(options.vulcanize.views || [], filename => {
    const taskName = 'vulc-views-' + filename;
    defineVulcanizeTask(taskName, path.join('src', 'views', filename));
    return taskName;
  }));

  gulp.task('vulcanizeFiles', gulpsync.sync(vulcanizeTasks));
  gulp.task('vulcanize', gulpsync.sync(['bower', 'less', 'pug', 'vulcanizeFiles']));

  gulp.task('customBuildClient');

  gulp.task('buildClient', gulpsync.sync(['buildShared', 'vulcanize', 'copyRes', 'customBuildClient']));

  // Build distribution

  const distDir = path.join(options.baseDir, 'dist');

  gulp.task('cleanDist', callback => {
    del.sync([distDir]);
    callback();
  });

  gulp.task('copyToDist', () =>
    gulp.src(['server/**/*', 'common/**/*', 'client/**/*', 'api/**/*', 'lib/**/*', 'generators/**/*', 'ui/**/*',
      'uiObject/**/*', 'scripts/**/*', 'state-machine/**/*', 'shared/**/*', 'config/**/*',
      'controllers/**/*', '*.json', '*.md', '*.js', 'src/**/*', 'docs/dist/**/*'
    ], {
      base: options.baseDir
    })
      .pipe(gulp.dest(distDir))
  );

  gulp.task('writeVersion', callback => {
    var dt = new Date();
    fs.writeFileSync(path.join(distDir, 'ng-rt-version'), weekOfYear(dt) + '.' + process.env.CI_PIPELINE_ID);
    callback();
  });

  gulp.task('zip', () =>
    gulp.src('dist/**/*')
      .pipe(zip(options.name + '.zip'))
      .pipe(gulp.dest(distDir))
  );

  gulp.task('clean', ['cleanDist', 'cleanClient']);
  gulp.task('dist', gulpsync.sync(['clean', 'buildClient', 'copyToDist', 'api', 'writeVersion',
    'zip']));
  gulp.task('build', ['buildClient']);
  gulp.task('rebuild', gulpsync.sync(['clean', 'build']));

  // by default, it creates zip package for distribution
  gulp.task('default', ['dist']);

  return {
    vulcanize: defineVulcanizeTask,
    copyToPublic: defineCopyToPublicTask,
    pugToPublic: definePugToPublicTask,
    bundleJs: defineBundleJsTask,
    copyJs: defineCopyJsTask
  };
};
