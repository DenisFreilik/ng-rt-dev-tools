#! /usr/bin/env node

/**
 * @author Michael.Reh
 * @desc Command line interface for ng-rt
 * @version 1.4
 * @copyright Tymlez Software een Consultancies BV 2016
 */

'use strict';

const result = require("./cli/result");
const bold = require("cli-colors").bold;
const argv = require('minimist')(process.argv.slice(2));
const i18n = require('i18n');
const logger = require('log4js').getLogger('cli');
const path = require('path');
const log4js = require('log4js');
const configService = require('./server/backend/configService');

const commands = {
  upload: require("./cli/commands/command")("pluginCommands/upload").default,
  activate: require("./cli/commands/command")("pluginCommands/activate"),
  deactivate: require("./cli/commands/command")("pluginCommands/deactivate"),
  install: require("./cli/commands/command")("pluginCommands/install"),
  uninstall: require("./cli/commands/command")("pluginCommands/uninstall"),
  delete: require("./cli/commands/command")("pluginCommands/remove"),
};

/**
 * Display help
 * @param {object}  i18n - The object instance of type i18n
 */
function help(i18n) {
  let helpText = bold("Usage:") + "\n" +
    "   Blockchain Enterprise Architecture Dev Tools <command> [--option=<argument>] [help]\n\n" +
    bold("Description:") + "\n" +
    "   Blockchain Enterprise Architecture Dev Tools command-line cli\n\n" +
    bold("Commands:\n") +
    "   upload [(--host and --port) or (--serverEnv), (--appKey)]\n\n" +
    "   delete [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]\n\n" +
    "   activate [(--host and --port) or (--serverEnv), (--appKey), (--pluginName), (--storage)]\n" +
    "   deactivate [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]\n\n" +
    "   install [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]\n\n" +
    "   uninstall [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]\n\n";
  logger.info(i18n.__(helpText));
}

// initialze multilingual support for server side string translation
let localFolder = path.join(__dirname, ".", "locales");
let i18nService = configService.get('i18n');
if (i18nService) {
  i18n.configure(i18nService);
} else {
  // use default settings
  i18n.configure({
    locales: ['en', 'de', 'ru', 'nl', 'fr'],
    directory: localFolder,
    defaultLocale: 'en',
    register: global
  });
}


let log4jsConfig = configService.get('log4js');
if (typeof log4jsConfig === 'object' && log4jsConfig.appenders) {
  log4jsConfig.appenders.forEach(appender => {
    if (appender.filename) {
      appender.filename = path.resolve(__dirname, appender.filename);
    }
  });
}
// logger.debug(log4jsConfig);
log4js.configure(log4jsConfig);

//logger.trace(i18n.__("Multilingual support started"));

let cmd = argv._.shift();

const command = commands[cmd];

if (command) {
  if (argv.h || argv.help) {
    result.help(i18n);
  } else {
    commands[cmd](argv, result, i18n).then(message => {
      logger.info(message);
    }).catch(err => {
      logger.error(err);
    });
  }
} else {
  help(i18n);
}
