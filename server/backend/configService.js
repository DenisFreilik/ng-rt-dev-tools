'use strict';
const fs = require('fs');
const path = require('path');
const nconf = require('nconf');
const Promise = require("bluebird");

var argv = require('minimist')(process.argv.slice(2));
var configFileName = argv['config-file'] || 'config.json';
var configPath = path.join(__dirname, '..', '..', "config", configFileName);

nconf
  .env()
  .argv();

function stopWatching() {
  fs.unwatchFile(configPath);
}

function startWatching() {
  fs.watchFile(configPath, () => {
    nconf.file({
      file: configPath
    });
  });
}

function reloadFile(path, watch) {
  stopWatching();

  nconf.file({
    file: path || configPath
  });

  if (watch)
    startWatching();
}

function toNconf(field) {
  return field.replace(new RegExp("\\.", 'g'), ":");
}

function get(field, defaultValue) {
  var value = nconf.get(toNconf(field));
  if (typeof value === 'undefined')
    return defaultValue;
  return value;
}

function add(field, value) {
  return new Promise(function(resolve, reject) {
    nconf.set(toNconf(field), value);
    nconf.save(function(err) {
      if (err) {
        return reject(new Error(err));
      }
      resolve(true);
    });
  });
}

function addMultiple(conf) {
  return new Promise(function(resolve, reject) {
    Object.keys(conf).forEach(function(field) {
      nconf.set(toNconf(field), conf[field]);
    });
    nconf.save(function(err) {
      if (err) {
        return reject(new Error(err));
      }
      resolve(true);
    });
  });
}

function remove(field) {
  return new Promise(function(resolve, reject) {
    nconf.clear(toNconf(field));
    nconf.save(function(err) {
      if (err) {
        return reject(new Error(err));
      }
      resolve(true);
    });
  });
}

reloadFile();

module.exports = {
  get: get,
  add: add,
  addMultiple: addMultiple,
  remove: remove,
  stopWatching: stopWatching,
  config: nconf,
  configFilePath: configPath,
  reloadFile: reloadFile
};
