Installation of the Development Tools
=====================================

Install it as a package dependency
----------------------------------

.. code:: bash

  $ npm i git+https://gitlab.tymlez.com/tymlez/ng-rt-dev-tools.git --save or --saveDev


Install it globally
-------------------

.. code:: bash

  $ npm i -g git+https://gitlab.tymlez.com/tymlez/ng-rt-dev-tools.git


Uninstall - if installed globally
---------------------------------

.. code:: bash

  $ npm -g uninstall ng-rt-dev-tools

Configuration
=============

After the installation of the SDK the configuration exists as a file ./config/config.json::

    {
      "i18n": {
        "locales": [
          "en",
          "de",
          "ru",
          "fr",
          "nl"
        ],
        "directory": "locales",
        "defaultLocale": "en",
        "register": "global"
      },
      "log4js": {
        "appenders": [{
          "type": "console",
          "layout": {
            "type": "pattern",
            "pattern": "%m"
          }
        },
          {
            "type": "dateFile",
            "pattern": "-from-MM-dd",
            "filename": "./log/ng-rt.log",
            "layout": {
              "type": "pattern",
              "pattern": "[%h %z %d %p %c] %m"
            }
          }
        ],
        "levels": {
          "[all]": "info"
        }
      },
      "env": {
        "default": "local",
        "local": {
          "serverType": "http",
          "serverUrl": "localhost:8443",
          "appKey": "5cl9b3ekms0t7llobqq4k53hgnd1pr3j9csj43shrk3o7m0s3mf"
        }
      }
    }


Work with the NG-RT-Development Tools
=====================================

Usage:

gulp <-task> --option=<argument>

Command:

* doc
* dist
* lint
* test.server
* test.ui
* userguides

option:

* gulp lint [(--src=<Folder or files>)]

.. note::

  --option(s) in brackets () are optional. All others are mandatory. Please add TWO hyphen in front of every option

usage of configuration options for userguides
---------------------------------------------

configuration options::

    const devTools = require('ng-rt-dev-tools').defineGulpTasks(gulp, {
      userguides: {
        includes: ['admin', 'enduser'],
        outputHtml: true,
        outputEpub: true,
        outputPdf: true,
      },
    });


Command line interface
======================

Manage some aspects of the lifecycle of a plugin in order to

.. warning::

  the cli is not integrated into a gulp task (yet) and an experimental feature only


Usage:

    node cli <-command> --option=<argument>

.. code:: bash

  $ node node_modules/ng-rt-dev-tools/cli upload --appKey lo0t7haksr1t3dn2462994b330nj3pejb88kka8le74dhob7


Command:

* upload ( incl. installation and activation )
* remove ( incl. de-installation, de-activation and deleting the plugin )
* activate
* de-activate
* install
* de-install

option:

* upload [(--host and --port) or (--serverEnv), (--appKey)]\n\n"
* delete [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]
* activate [(--host and --port) or (--serverEnv), (--appKey), (--pluginName), (--storage)]
* deactivate [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]
* install [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]
* uninstall [(--host and --port) or (--serverEnv), (--appKey),(--pluginName), (--storage)]

.. note::

  --option(s) in brackets () are optional. All others are mandatory. Please add TWO hyphen in front of every option

.. note::

  if --serverEnv is NOT being specified the default environment incl. the specific key-pairs might be used

* --appkey needs to be registered against ng-rt-admin of the system on which you want to manage the plugins
* --pluginName = Name of the plugin to be upload. if not specified the zip in folder dist is being used
* --serverEnv = defined in config.json
* --serverType = http or https
* --serverUrl = url (i.e. playground.tymlez.com or localhost:8443)
* --host = IP address of the Blockchain server
* --port = Port of the Blockchain server
* --serverEnv = a specific serverEnv
* --storage (main or customPlugins)
