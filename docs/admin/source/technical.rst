.. raw:: latex

    \clearpage

Technical Information
=====================

Installation
------------

.. note::

  The installation of the plugin can be done manually on CLI only. Only Macintosh computers as clients are supported for now.


Pre-requisites
--------------

.. warning::

  Google Chrome Version 57.0.2987.133 or higher is supported for this application


Technical details
-----------------

Various configuration options can be configured in the configuration :file:`config.json`.
