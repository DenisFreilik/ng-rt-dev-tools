\select@language {english}
\contentsline {chapter}{\numberline {1}Who should read this guide}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Introduction}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}NG-RT Development Tools}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Installation of the Development Tools}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Install it as a package dependency}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Install it globally}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Uninstall - if installed globally}{5}{section.3.3}
\contentsline {chapter}{\numberline {4}Configuration}{7}{chapter.4}
\contentsline {chapter}{\numberline {5}Work with the NG-RT-Development Tools}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}usage of configuration options for userguides}{9}{section.5.1}
\contentsline {chapter}{\numberline {6}Command line interface}{11}{chapter.6}
\contentsline {chapter}{\numberline {7}Technical Information}{13}{chapter.7}
\contentsline {section}{\numberline {7.1}Installation}{13}{section.7.1}
\contentsline {section}{\numberline {7.2}Pre-requisites}{13}{section.7.2}
\contentsline {section}{\numberline {7.3}Technical details}{13}{section.7.3}
\contentsline {chapter}{\numberline {8}Technical diagrams}{15}{chapter.8}
\contentsline {section}{\numberline {8.1}Development Tools UML diagrams}{15}{section.8.1}
\contentsline {chapter}{\numberline {9}Legal Disclaimers and Legal Information}{17}{chapter.9}
