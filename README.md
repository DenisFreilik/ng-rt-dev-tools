# Tymlez ng-rt platform development tools

## Install it globally

git@gitlab.tymlez.com:tymlez/ng-rt-dev-tools.git
```
npm i -g git+https://gitlab.tymlez.com/tymlez/ng-rt-dev-tools.git
```
## uninstall

```
npm -g uninstall ng-rt-dev-tools
```

## tasks
supported gulp tasks:

gulp doc

gulp dist

gulp lint

gulp test.server

gulp test.ui

gulp userguides

## do linting for a specific folder
gulp lint --src "cli/**/*"

## testing

* To run tests:

```shell
npm test
```

* To test coverage:

```shell
npm run coverage
```
